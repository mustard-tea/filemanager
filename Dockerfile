FROM richarvey/nginx-php-fpm:1.4.0

RUN rm index.php
RUN wget http://static.kodcloud.com/update/download/kodexplorer4.25.zip
RUN unzip kodexplorer4.25.zip
RUN chmod -Rf 777 ./*